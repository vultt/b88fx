package vn.jav.dev.dto;

import java.io.Serializable;

import vn.jav.dev.catalog.BET_TYPES;
import vn.jav.dev.catalog.SELECTION;
import vn.jav.dev.catalog.TYPE_OF_MATCH;

public class BetDto implements Serializable {

    private static final long serialVersionUID = -5520316067297777417L;

    public SELECTION selection;
    public TYPE_OF_MATCH typeOfMatch;
    public BET_TYPES typeOfBet;
    public String home;
    public String away;
    public String league;
    public String matchId;
    public String hdpPoint;
    public String betTime;
    public Double moneyBid;

}
